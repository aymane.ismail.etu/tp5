import Router from './Router';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';
import $ from 'jquery';

$('.logo').append("<small>les pizzas c'est la vie");

const pizzaList = new PizzaList([]),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.$titleElement = $('.pageTitle');
Router.$contentElement = $('.pageContent');
Router.$menuElement = $('.mainMenu');

// History API (gestion des boutons précédent/suivant du navigateur)
window.onpopstate = () => {
	Router.navigate(document.location.pathname, false);
};
// deep linking
Router.navigate(document.location.pathname);

// TP4 - B.2. Charger un fichier statique
function displayNews(html) {
	const $newsContainer = $('.newsContainer');
	// injection du contenu chargé dans la page
	$newsContainer.html(html);
	// affichage du bandeau de news
	$newsContainer.css('display', '');
	// gestion du bouton fermer
	const $closeButton = $('.closeButton', $newsContainer);
	$closeButton.on('click', event => {
		event.preventDefault();
		$newsContainer.css('display', 'none');
	});
}
$.ajax('./news.html').done(displayNews);

$('.logo').on('click', event => {
	event.preventDefault();
	Router.navigate('/');
});

/*$('section a', document.querySelector('.pizzaList')).on('click', event => {
	event.preventDefault();
	console.log(event.currentTarget);
});*/
